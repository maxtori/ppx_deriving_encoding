all: build

build:
	dune build src

dev:
	dune build test

clean:
	dune clean
